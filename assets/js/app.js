import Vue from 'vue';
import App from './components/App.vue';
import HeaderComp from './components/commons/HeaderComp';
import SubHeader from './components/commons/SubHeader';
import Articles from './components/commons/Articles';
import hotEmitter from 'webpack/hot/emitter';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
/**
 * Create a fresh Vue Application instance
 */
Vue.component("HeaderComp", HeaderComp);
Vue.component("SubHeader", SubHeader);
Vue.component("Articles", Articles);

new Vue({
    el: '#app',
    render: h => h(App),
    beforeMount: function(){
        this.compras = JSON.parse(this.$el.attributes['data-name'].value);
    },
});


Vue.use(BootstrapVue);

//DEV: HMR for css
if (process.env.NODE_ENV === 'development') {
    if (module.hot) {
        hotEmitter.on('webpackHotUpdate', () => {
            document.querySelectorAll('link[href][rel=stylesheet]').forEach((link) => {
                link.href = link.href.replace(/(\?\d+)?$/, `?${Date.now()}`)
            })
        })
    }
}